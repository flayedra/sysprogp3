#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUM_PERSONAS 10
#define SIZE 30
#define SIZEdad 3

int validoNombre(char *);
int validoEdad (char *);
float edad_promedio(int *array);
void imprimir_nombres(char **nombres);
int mayor_edad(int *edades);
int menor_edad(int *edades);
int edades[10];

int main()
{
   	char nombres[NUM_PERSONAS][SIZE];
	char apellidos[NUM_PERSONAS][SIZE];	
	
	for (int i =0; i<NUM_PERSONAS ; i++)
        {
                char name[SIZE];
                do{
                        printf ("Ingrese su nombre:");
                        scanf ("%s", name);
                }while (validoNombre(name)==0);
		
		strcpy(nombres[i],name);
                memset(name,'\0',30*sizeof(char));
                        
		char surname[SIZE];
		do{
			printf ("Ingrese su apellido:");
                        scanf ("%s", surname);
                }while (validoNombre(surname)==0);
		
		strcpy(apellidos[i],surname);
                memset(surname,'\0',30*sizeof(char));

                char  edad[SIZEdad];
                do{
                        printf ("Ingrese su edad:");
                        scanf ("%s", edad);
                }while (validoEdad(edad)==0);
		
		edades[i] = atoi(edad);		
        }


	float edadProm =  edad_promedio(edades);
	int max_index = mayor_edad(edades);
	int min_index = menor_edad(edades);

	printf("Promedio de Edad =  %.2f\n",edadProm);
	printf("Persona más vieja:  %s %s, edad = %d \n",nombres[max_index], apellidos[max_index],edades[max_index]);
	printf("Persona más joven:  %s %s, edad %d \n",nombres[min_index], apellidos[min_index],edades[min_index]);

	return(0);
}

int validoNombre (char *str)
{
	for(int i=0;i<SIZE; i++)
	{
		char c= str[i];
		if (c==0)
			break;
		/*
		Primera iteración mayúsculas.
		*/	
		if (i==0 &&( c< 65 || c>90))
		{
			return 0;
		}
		/*
		Iteración minúsculas.
                */
		if (i>0 && (c<97 || c>122))
		{
			return 0;
		}

	}
	return 1;
}

int validoEdad (char *str)
{
	//Valida si es número
        for(int i=0;i<SIZE; i++)
        {
                char c= str[i];
                if (c==0)
                        break;
                if ( c< 48 || c>57)
                {
                        return 0;
                }
        }
	//Valida si está dentro del rango
    	int i;
	i  = atoi (str);  
	if (i<130 && i > 0){
		return 1;
	}

	return 0;
}


float edad_promedio(int *array){
	int i ;
	float sum = 0;
	for(i= 0 ; i < NUM_PERSONAS; i++){
		sum = sum + array[i];
	}
	return sum/NUM_PERSONAS;
}


int mayor_edad(int *edades){
	int max_index = 0 ;
	int i ;
	for( i= 1; i <NUM_PERSONAS ; i++){
		if (edades[i] > edades[max_index]){
			max_index = i;
		}
	}
	return max_index;
}

int menor_edad(int *edades){
	int min_index = 0 ;
	int i ;
	for( i= 1; i <NUM_PERSONAS ; i++){
		if (edades[i] < edades[min_index]){
			min_index = i;
		}
	}
	return min_index;
}
